//
//  InfinityTableViewController.swift
//  Chuck Norris Banter
//
//  Created by Tom Holmes @ TAE on 27/06/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import UIKit

class InfinityTableViewController: UITableViewController {
    
    var model: JokesModel!
    var networkManager: NetworkManager!
    
    var indicator = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()

        model = JokesModel()
        networkManager = NetworkManager()
        
        tableView.allowsSelection = false
        
        loadingStartAnimation()
        
        networkManager.delegate = self
        networkManager.getRandomJokes()
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfJokes()
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! JokeTableViewCell

        let joke = model.jokes[indexPath.row]
        cell.jokeLabel.text = joke

        return cell
    }
    

    // MARK: Table view delegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let count = model.numberOfJokes()
        
        if indexPath.row == count - 1 {
            networkManager.getRandomJokes()
            loadingStartAnimation()
        }
    }
    
    // MARK: UI Funcs
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        // PLEASE READ: When rotating the tableView to horizontal in the simlator the cells seem to keep their relative positions automatically, however, I am told on a real iPhone that this does not happen and becasue I do not have a real iPhone to test on I am not 100% sure this func's code is being excuted properly, I am operating off of blind faith here.
        super.viewWillTransition(to: size, with: coordinator)
        
    }
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    func loadingStartAnimation() {
        activityIndicator()
        indicator.startAnimating()
        indicator.backgroundColor = .white
    }
    
    func loadingStopAnimation() {
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
    }

}


// MARK: NetworkManagerDelegate
extension InfinityTableViewController: NetworkManagerDelegate {
    
    func didGetData(_ networkManager: NetworkManager, results: Results) {
        DispatchQueue.main.async {
            for result in results.value {
                self.model.jokes.append(result.joke)
            }
            
            self.loadingStopAnimation()
            self.tableView.reloadData()
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
    
    
}
