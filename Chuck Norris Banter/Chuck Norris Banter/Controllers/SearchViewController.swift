//
//  SearchViewController.swift
//  Chuck Norris Banter
//
//  Created by Tom Holmes @ TAE on 26/06/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var jokeText: UILabel!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    var networkManager: NetworkManager!
    var joke = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        jokeText.text = "Please enter valid firstname and lastname. Only letters are allowed."
        
        searchButton.layer.cornerRadius = 5.0
        
        networkManager = NetworkManager()
        networkManager.delegate = self
        
    }
    
    
    @IBAction func searchPressed(_ sender: UIButton) {
        // Force unwrapping here as even if a text field is empty it returns an empty string, not nil
        let firstName = firstNameField.text!
        let lastName = lastNameField.text!
        
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z]+$")
            
            if (regex.firstMatch(in: firstName, options: [], range: NSMakeRange(0, firstName.utf16.count)) != nil)
                && (regex.firstMatch(in: lastName, options: [], range: NSMakeRange(0, lastName.utf16.count)) != nil) {
                
                networkManager.getNamedJoke(firstName, lastName)
                
            } else {
                jokeText.text = "Please enter valid firstname and lastname. Only letters are allowed."
                jokeText.textColor = .red
            }
            
        } catch {
            print(error)
        }
        
    }
    
    func updateJoke() {
        jokeText.text = joke
        jokeText.textColor = .black
    }
    
}

extension SearchViewController: NetworkManagerDelegate {
    
    func didGetData(_ networkManager: NetworkManager, results: Results) {
        DispatchQueue.main.async {
            self.joke = results.value[0].joke
            self.updateJoke()
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
    
    
    
}
