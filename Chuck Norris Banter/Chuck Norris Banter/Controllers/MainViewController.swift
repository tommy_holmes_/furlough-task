//
//  ViewController.swift
//  Chuck Norris Banter
//
//  Created by Tom Holmes @ TAE on 26/06/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var randomButton: UIButton!
    @IBOutlet weak var textInputButton: UIButton!
    @IBOutlet weak var neButton: UIButton!
    
    var networkManager: NetworkManager!
    var randomJoke = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiSetUp()
        networkManager = NetworkManager()
        
        networkManager.delegate = self
    }
    
    
    // MARK: Actions
    @IBAction func randomPressed(_ sender: UIButton) {
        networkManager.getRandomJokes()
    }
    
    @IBAction func textPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toSearch", sender: self)
    }
    
    @IBAction func neverEndingPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toInfinity", sender: self)
    }
    
    @IBAction func explicitToggled(_ sender: UISwitch) {
        isExplicit.toggle()
    }
    
    // MARK: UI Funcs
    func uiSetUp() {
        randomButton.layer.cornerRadius = 5.0
        textInputButton.layer.cornerRadius = 5.0
        neButton.layer.cornerRadius = 5.0
    }
    
    func presentRandomJoke() {
        let alertController = UIAlertController()
        
        alertController.message = randomJoke
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }

}


// MARK: NetworkManagerDelegate

extension MainViewController: NetworkManagerDelegate {
    
    func didGetData(_ networkManager: NetworkManager, results: Results) {
        DispatchQueue.main.async {
            self.randomJoke = results.value[0].joke
            self.presentRandomJoke()
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
    
    
}
