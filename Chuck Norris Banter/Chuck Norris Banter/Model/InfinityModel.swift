//
//  InfinityModel.swift
//  Chuck Norris Banter
//
//  Created by Tom Holmes @ TAE on 27/06/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import Foundation

class JokesModel {
    
    var jokes: [String]
    
    init() {
        jokes = []
    }
    
    
}


extension JokesModel {
    
    func numberOfJokes() -> Int {
        jokes.count
    }
    
    
}
