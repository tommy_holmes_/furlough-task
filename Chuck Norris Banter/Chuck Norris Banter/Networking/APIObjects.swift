//
//  APIObjects.swift
//  Chuck Norris Banter
//
//  Created by Tom Holmes @ TAE on 26/06/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import Foundation

struct Results: Decodable {
    var value: [Value]
}

struct Value: Decodable {
    var joke: String
}

