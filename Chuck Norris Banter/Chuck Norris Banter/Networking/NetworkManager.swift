//
//  NetworkManager.swift
//  Chuck Norris Banter
//
//  Created by Tom Holmes @ TAE on 26/06/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import Foundation

public var isExplicit = true

protocol NetworkManagerDelegate {
    func didGetData(_ networkManager: NetworkManager, results: Results)
    func didFailWithError(error: Error)
}

class NetworkManager {
    
    var root: Results?
    var delegate: NetworkManagerDelegate?
    
    var exclusion: String {
        if isExplicit {
            return ""
        } else {
            return "exclude=[explicit]"
        }
    }
    
    func getRandomJokes() {
        
        var comps = URLComponents()
        
        comps.scheme = "https"
        comps.host = "api.icndb.com"
        comps.path = "/jokes/random/15\(exclusion)"
        
        guard let url = comps.url else {
            print("URL Error")
            return
        }
        
        getResults(with: url)
    }
    
    func getNamedJoke(_ firstName: String, _ lastName: String) {
        
        var comps = URLComponents()
        
        comps.scheme = "https"
        comps.host = "api.icndb.com"
        comps.path = "/jokes/random/1\(exclusion)"
        comps.queryItems = [URLQueryItem(name: "firstName", value: firstName),
                            URLQueryItem(name: "lastName", value: lastName)
        ]
        
        guard let url = comps.url else {
            print("URL Error")
            return
        }
        
        getResults(with: url)
    }
    
    func getResults(with url: URL) {
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                print("Error - \(error.localizedDescription)")
            } else if let response = response as? HTTPURLResponse {
                
                switch response.statusCode {
                case 200...299:
                    if let data = data {
                        if let results = self.parseJSON(data) {
                            self.delegate?.didGetData(self, results: results)
                            
                        }
                    }
                default:
                    if let error = error {
                        self.delegate?.didFailWithError(error: error)
                        
                    }
                }
            }
        }
        
        dataTask.resume()
    }
    
    
    func parseJSON(_ data: Data) -> Results? {
        let jsonDecoder = JSONDecoder()
        
        do {
            let results = try jsonDecoder.decode(Results.self, from: data)
            root = results
            
            return results
            
        } catch {
            print(error)
            return nil
        }
        
    }
    
    
}
