//
//  Chuck_Norris_BanterTests.swift
//  Chuck Norris BanterTests
//
//  Created by Tom Holmes @ TAE on 26/06/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import XCTest
@testable import Chuck_Norris_Banter

class Chuck_Norris_Banter_MainVC_Tests: XCTestCase {
    
    //var sut: MainViewController?
    
    override func setUp() {
        super.setUp()
        
        
        //sut = MainViewController()
    }
    
    override func tearDown() {
        //sut = nil
        
        super.tearDown()
    }

    func test_presentRandomJokeAC() {
        //sut?.presentRandomJoke()
        
        let testExpectation = expectation(description: "testExample")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            //XCTAssert(self.sut?.presentingViewController is UIAlertController)
            testExpectation.fulfill()
        })
        wait(for: [testExpectation], timeout: 1.5)
    }

}

class SearchVC_Tests: XCTestCase {
    
    var sut: SearchViewController?
    
    override func setUp() {
        super.setUp()
        
        sut = SearchViewController()
        
        
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    func test_regex_invalidCharacter_firstname() {
//        sut?.searchButton.sendActions(for: .allTouchEvents)
        
        let expectation = self.expectation(description: "Invalid")
        defer { wait(for: [expectation], timeout: 1.0) }
        
        let firstName = "Tom£"
        let lastName = "Holmes"
        
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z]+$")
            
            if (regex.firstMatch(in: firstName, options: [], range: NSMakeRange(0, firstName.utf16.count)) != nil)
                && (regex.firstMatch(in: lastName, options: [], range: NSMakeRange(0, lastName.utf16.count)) != nil) {
                
                XCTFail()
                
            } else {
                expectation.fulfill()
            }
            
        } catch {
            XCTFail()
        }
        
    }
    
    func test_regex_invalidCharacter_lastname() {
        
        let expectation = self.expectation(description: "Invalid")
        defer { wait(for: [expectation], timeout: 1.0) }
        
        let firstName = "Tom"
        let lastName = "Holmes$"
        
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z]+$")
            
            if (regex.firstMatch(in: firstName, options: [], range: NSMakeRange(0, firstName.utf16.count)) != nil)
                && (regex.firstMatch(in: lastName, options: [], range: NSMakeRange(0, lastName.utf16.count)) != nil) {
                
                XCTFail()
                
            } else {
                expectation.fulfill()
            }
            
        } catch {
            XCTFail()
        }
        
    }
    
    func test_regex_validNames() {
        
        let expectation = self.expectation(description: "Invalid")
        defer { wait(for: [expectation], timeout: 1.0) }
        
        let firstName = "Tom"
        let lastName = "Holmes"
        
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z]+$")
            
            if (regex.firstMatch(in: firstName, options: [], range: NSMakeRange(0, firstName.utf16.count)) != nil)
                && (regex.firstMatch(in: lastName, options: [], range: NSMakeRange(0, lastName.utf16.count)) != nil) {
                
                expectation.fulfill()
                
            } else {
                XCTFail()
            }
            
        } catch {
            XCTFail()
        }
        
    }
    
    func test_regex_noNames() {
        
        let expectation = self.expectation(description: "Invalid")
        defer { wait(for: [expectation], timeout: 1.0) }
        
        let firstName = ""
        let lastName = ""
        
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z]+$")
            
            if (regex.firstMatch(in: firstName, options: [], range: NSMakeRange(0, firstName.utf16.count)) != nil)
                && (regex.firstMatch(in: lastName, options: [], range: NSMakeRange(0, lastName.utf16.count)) != nil) {
                
                XCTFail()
                
            } else {
                expectation.fulfill()
            }
            
        } catch {
            XCTFail()
        }
        
    }
    
}


class Networking_Tests: XCTestCase {
    
    func test_noServerResponse() {
        let expectation = self.expectation(description: "Sever responds in reasonable time")
        defer { wait(for: [expectation], timeout: 2.0) }
        
        let url = URL(string: "chuckNorris")!
        URLSession.shared.dataTask(with: url) { data, response, error in
            XCTAssertNil(data)
            XCTAssertNil(response)
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
    .resume()
        
    }
    
    func test_getNamedJoke_serverResponse_Explicit() {
        let expect = expectation(description: "Sever responds in reasonable time")
        defer { wait(for: [expect], timeout: 2.0) }
        
        var comps = URLComponents()
        
        comps.scheme = "https"
        comps.host = "api.icndb.com"
        comps.path = "/jokes/random/1"
        comps.queryItems = [URLQueryItem(name: "firstName", value: "Tom"),
                            URLQueryItem(name: "lastName", value: "Holmes")
        ]
        
        let url = comps.url!
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            XCTAssertNotNil(data)
            XCTAssertNotNil(response)
            XCTAssertNil(error)
            expect.fulfill()
        }
    .resume()
        
    }
    
    func test_getNamedJoke_serverResponse_notExplicit() {
        let expect = expectation(description: "Sever responds in reasonable time")
        defer { wait(for: [expect], timeout: 2.0) }
        
        var comps = URLComponents()
        
        comps.scheme = "https"
        comps.host = "api.icndb.com"
        comps.path = "/jokes/random/1exclude=[explicit]"
        comps.queryItems = [URLQueryItem(name: "firstName", value: "Tom"),
                            URLQueryItem(name: "lastName", value: "Holmes")
        ]
        
        let url = comps.url!
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            XCTAssertNotNil(data)
            XCTAssertNotNil(response)
            XCTAssertNil(error)
            expect.fulfill()
        }
    .resume()
        
    }
    
    func test_getRandomJoke_serverResponse_Explicit() {
        let expect = expectation(description: "Sever responds in reasonable time")
        defer { wait(for: [expect], timeout: 2.0) }
        
        var comps = URLComponents()
        
        comps.scheme = "https"
        comps.host = "api.icndb.com"
        comps.path = "/jokes/random/15"
        
        let url = comps.url!
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            XCTAssertNotNil(data)
            XCTAssertNotNil(response)
            XCTAssertNil(error)
            expect.fulfill()
        }
    .resume()
        
    }
    
    func test_getRandomJoke_serverResponse_notExplicit() {
        let expect = expectation(description: "Sever responds in reasonable time")
        defer { wait(for: [expect], timeout: 2.0) }
        
        var comps = URLComponents()
        
        comps.scheme = "https"
        comps.host = "api.icndb.com"
        comps.path = "/jokes/random/15exclude=[explicit]"
        
        let url = comps.url!
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            XCTAssertNotNil(data)
            XCTAssertNotNil(response)
            XCTAssertNil(error)
            expect.fulfill()
        }
    .resume()
        
    }
    
}
