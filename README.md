# Furlough Task


* Home VC consists of 3 UIButtons for each of the 3 feature below, a UISwitch and a UILabel displaying what the switch does. 
* Toggling the switch toggles a corresponding Bool in the Network Manager that then is used in a calculated var that is a string to be appended to the URL for the network call that either excludes or includes explicit jokes depending on the state of the switch. 

1) Random Joke
When the 􏰀Rand􏰁m Jo􏰁ke􏰂 button is pressed, the app fetches one random joke from the Chuck Norris server and displays the random joke in a popup with an OK button.

* Simply presents a UIAlertController with the first result from the network call as its message. 

2) Text input
When the 􏰀Text input􏰂 button is pressed, the app opens a new view with a text field and a 􏰀Search􏰂 Button. Upon pressing the 􏰀Search􏰂 Button, the app should request a random joke with a custom main character as described in the 􏰃Changing the name of the main character􏰄 section of the API docs and show it in a popup.
First name/last name splitting and input validation needs to be considered.

* Segues to new VC with two textfields for first and last name respectivly.
* Once search button pressed, an 'if' statement uses a regex to check if both names are made of letters only before making the network call, if not instructions get highlighted in red as a visual que for user. 
* Network call uses a func that takes first and last names as arguments which are then used to construct the URL for the GET request. 
* Returned joke string gets used at text that replaces the instructions on the label above the text fields. 

3) Never-ending Joke list
When the 􏰀Never-ending Jo􏰁ke􏰅􏰂 button is pressed, the app opens a new view that contains a list of random jokes. Jokes should be requested asynchronously, in batches from the server. When the user scrolls to the bottom of the list, the list shows a loading message indicating that more jokes are being fetched.
Since the jokes are random, it is fine in this simple task to have duplicate jokes in the list.

* Segues to UITableViewController that initally displays 15 jokes from the API, but if you scroll to the last cell it calls the delegate func 'willDisplay cell' to then call the network API again to load and append another 15 results to an array for all the jokes used in the data source. 
* UIActivityIndicator displayed when fetching API results to tell user that results are loading, called in both the viewDidLoad and the 'willDisplay Cell' funcs and dismissed in the 'didGetData' network manger delegate func.   
